class WelcomeController < ApplicationController
  def index
  end

  def about
  end

  def hiring
  end

  def blog
  end

  def contact
  end
end
