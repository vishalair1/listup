jQuery(document).ready(function ($) {
    "use strict";
	

	
	
	// Sticky Header
$(window).scroll(function() {

    if ($(window).scrollTop() > 600) {
        $('.header').addClass('sticky');
    } else {
        $('.header').removeClass('sticky');
    }
});


	
   
    /* --------------------------------
     swipebox video light box
     -------------------------------- */


    $('.swipebox-video').swipebox();
     $('.swipebox').swipebox();



    /* --------------------------------
     Carousel
     -------------------------------- */

    // $('#Carousel').carousel({
    //     interval: 5000
    // });

    
    /* --------------------------------
     screenshot carousel
     -------------------------------- */

    $('.carousel[data-type="multi"] .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i=0;i<2;i++) {
            next=next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });


    //Enable touch swiping for carousel
    $(".carousel-inner").swipe( {
        //Generic swipe handler for all directions
        swipeLeft:function(event, direction, distance, duration, fingerCount) {
            $(this).parent().carousel('prev');
        },
        swipeRight: function() {
            $(this).parent().carousel('next');
        },
        //Default is 75px, set to 0 for demo so any distance triggers swipe
        threshold:0
    });


    /* --------------------------------
     Typer.js is built by Layervault
     -------------------------------- */

    $('[data-typer-targets]').typer();

  
    /*----------------------------------------------------*/
    /*  Input field animation
     /*----------------------------------------------------*/

    (function () {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call(document.querySelectorAll('input.input__field, textarea.input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }

            // events:
            inputEl.addEventListener('focus', onInputFocus);
            inputEl.addEventListener('blur', onInputBlur);
        });

        function onInputFocus(ev) {
            classie.add(ev.target.parentNode, 'input--filled');
        }

        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'input--filled');
            }
        }
    })();
   

});




